/*
Parte 1:
Hacer la implementación de una lista enlazada.
Debe haber funciones para:
-Crear e inicializar la lista
-Agregar un elemento
-Obtener el largo de la lista
-Obtener un elemento N de la lista
-Eliminar un elemento N de la lista
-Imprimir la lista

Parte 2:
Implementar una lista de enteros ordenada.
Cada elemento que agrego queda ordenado en la lista,
de manera que al imprimirla se imprime automáticamente ordenada.

Fecha de entrega (parte 1 + parte 2): 2023-05-13
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

typedef struct
{
    int numero;
} Cont;

typedef struct Elemento
{
    Cont dato;
    struct Elemento *siguiente;
} Nodo;

// crear e inicializar la lista
Nodo *crearLista()
{
    Nodo *head = NULL;
    return head;
}

Nodo *crearElemento(Cont x)
{
    Nodo *a;
    a = malloc(sizeof(Nodo));
    a->dato = x;
    a->siguiente = NULL;
    return a;
}

// obtener el largo de la lista
int size(Nodo *head)
{
    Nodo *actual;
    int cantElementos = 0;
    for (actual = head; actual != NULL; actual->siguiente)
        cantElementos++;
    return cantElementos;
}

// obtener el elemento n de la lista
Nodo *buscar(Nodo *head, Cont datoBuscado)
{
    Nodo *actual;
    for (actual = head; actual != NULL; actual->siguiente)
        if (datoBuscado.numero == actual->dato.numero)
            return actual;
    return NULL;
}

// Eliminar un elemento de la lista
void eliminar(Nodo **head, Cont valor)
{
    Nodo *actual = *head;
    Nodo *anterior = NULL;
    while ((actual != NULL) && (actual->dato.numero != valor.numero))
    {
        anterior = actual;
        actual = actual->siguiente;
    }
    if (actual == NULL)
    {
        if (*head == NULL)
            printf("La lista esta vacia\n");
        else
            printf("No se encontro el elemento en la lista\n");
    }
    else
    {
        if (*head == actual)
            *head = actual->siguiente;
        else
            anterior->siguiente = actual->siguiente;
        free(actual);
    }
}

// imprimir la lista
void imprimirLista(Nodo *head)
{
    Nodo *actual;
    printf("La lista es:\n");
    printf("%d", head->dato.numero);
    for (actual = head->siguiente; actual != NULL; actual = actual->siguiente)
        printf("%s %d", ",", actual->dato.numero);
    printf("\n\n");
}

// insertar
void insertarInicio(Nodo **head, Cont valor)
{
    Nodo *nuevo;
    nuevo = crearElemento(valor);
    nuevo->siguiente = *head;
    *head = nuevo;
}

void insertar(Nodo **head, Nodo *anterior, Cont valor)
{
    if (anterior == NULL || *head == NULL)
        insertarInicio(head, valor);
    else
    {
        Nodo *nuevo = crearElemento(valor);
        nuevo->siguiente = anterior->siguiente;
        anterior->siguiente = nuevo;
    }
}

void insertarOrdenado(Nodo **head, Cont x)
{
    if (*head == NULL)
        insertarInicio(head, x);
    else
    {
        Nodo *actual = *head;
        Nodo *anterior = NULL;
        while ((actual != NULL) && (actual->dato.numero <= x.numero))
        {
            anterior = actual;
            actual = actual->siguiente;
        }
        insertar(head, anterior, x);
    }
}

int main()
{
    Nodo *lista = crearLista();
    int i;
    Cont x;
    for (i = 0; i < 20; i++)
    {
        x.numero = i;
        insertarOrdenado(&lista, x);
    }

    imprimirLista(lista);
    // int aux = lista.size();
    for (i = 0; i < 20; i += 2)
    {
        x.numero = i;
        eliminar(&lista, x);
    }
    Cont a;
    a.numero = 2;
    insertarOrdenado(&lista, a);

    Cont b;
    b.numero = -1;
    insertarOrdenado(&lista, b);

    Cont c;
    c.numero = 20;
    insertarOrdenado(&lista, c);

    imprimirLista(lista);
    return 0;
}